import './env';
import db from './db';
import { CreateServer } from "./server";

const server = CreateServer(db);

const port = process.env.PORT || 4000;
server.listen({ port }).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`)
});
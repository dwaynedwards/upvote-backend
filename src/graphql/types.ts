export interface IContext {
  db: any;
  req: any;
}

export interface IUser {
  id: string
  firstName: string
  lastName: string
  email: string
  isActive: boolean
  createdAt: string
  updatedAt: string
}

export interface IAuthPayload {
  token: string
}
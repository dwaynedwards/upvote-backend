import { MutationResolvers } from "../generated/graphqlgen";

export const Mutation: MutationResolvers.Type = {
  ...MutationResolvers.defaultResolvers,
  register: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  login: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};

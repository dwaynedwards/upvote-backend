import { join } from 'path';

import * as Knex from "knex";

import { ApolloServer, gql } from 'apollo-server';
import { importSchema } from 'graphql-import'

const typeDefs = importSchema(join(__dirname, './graphql/schemas/schema.graphql'));
import { resolvers } from "./graphql/resolvers";

export const CreateServer = (db: Knex): ApolloServer => {
  const server = new ApolloServer({
    typeDefs: gql(typeDefs),
    resolvers,
    playground: true,
    context: req => {
      return {
        ...req,
        db,
      };
    }
  });
  return server;
}
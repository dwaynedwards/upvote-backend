import * as Knex from "knex";
import * as knexConfig from "../knexfile";

const env = process.env.NODE_ENV || 'development';
const db = Knex(knexConfig[env]);
export default db;
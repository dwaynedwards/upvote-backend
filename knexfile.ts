import { join } from 'path';

import * as Knex from "knex";

interface IKnexConfig {
  development: Knex.Config,
  [key: string]: Knex.Config,
}

const knexConfig: IKnexConfig = {
  development: {
    client: 'pg',
    connection: {
      port: process.env.PG_PORT,
      host: process.env.PG_HOST,
      database: process.env.PG_DB,
      user: process.env.PG_USER,
      password: process.env.PG_PASSWORD,
    },
    migrations: {
      directory: join(__dirname, './db/migrations'),
      tableName: 'knex_migrations',
      extensions: ['js', 'ts'],
    },
  } as Knex.Config,
}

export = knexConfig;
#!/usr/bin/env bash

script_path="$(cd "$(dirname "$0")" && pwd -P)"
knexfile_path="$(dirname $script_path)/knexfile.ts"

setEnvironment() {
  if [[ -n "$1" ]]; then
    ENV="$1"
  else
    ENV="development"
  fi
}

if [[ "$1" = "make" && -n "$2" ]]; then
  filename=$2
  setEnvironment $3
  knex migrate:make --knexfile $knexfile_path -x ts $filename --env $ENV
  exit 0
fi

if [[ "$1" = "latest" ]]; then
  setEnvironment $2
  knex migrate:latest --knexfile $knexfile_path --env $ENV
  exit 0
fi

echo "No matched arguments: $@"
exit 3